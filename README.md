# citeproc-cli

_Requires_ `node>=12.0.0`

Rather simplistic command line wrapper around citeproc-js. It can be packaged by pkg to provide citeproc functionality with a simple api in a portable form.

Since my use case is a Markdown conversion library, this tool renders citations and bibliography as HTML.

## Flags

#### `--data`/`-d`

_Required_

Path to a text file containing your bibliographic library in CSLJSON.

#### `--style`/`-s`

_Required_

Path to a CSL file containing your bibliographic style in XML format.

#### `--bibliography`/`-b`

_Default:_ `false`

Render bibliography (omit to render citations to use in footnotes/endnotes)

#### `--localedir`/`-j`

_Default:_ `./locales`

Path to a directory containing CSL locale files in XML format. Ideally, this directory would be a clone of the [CSL locale repository](https://github.com/citation-style-language/locales).

#### `--locale`/`-l`

_Default:_ `en-US`

Desired locale as [RFC 5646](https://datatracker.ietf.org/doc/html/rfc5646) compliant string.

#### `--keys`/`-k`

_Default:_ `[]`

JSON-serialized list containing cited keys (without a preceding `@`). Either wrap in single quotes (which should always be possible since allowed characters in CSL keys are limited) or avoid white space. One list entry can contain multiple keys separated by `;`. This flag is overridden by `--references`/`-r` and `--reffile`/`-p`.

_Example:_

```bash
citeproc-cli -k '["citekey1", "citekey2;citekey3", "citekey4"]'
```

#### `--references`/`-r`

_Default:_ `[]`

JSON serialized array of arrays of dictionaries containing references. Every dictionary must contain at least the key `ìd` (value: citekeys without a preceding `@`) but can contain additional keys to pass to the citation processor. The deserialized dictionaries are directly passed as `citationItems` and can therefore contain any of the keys mentioned in the [citeproc-js documentation](https://github.com/Juris-M/citeproc-js/blob/master/attic/citeproc-doc.rst#citation-data-object). Each array of dictionaries is treated as one citation. This flag overrides `--keys`/`-k` and is overridden by `--reffile`/`-p`.

_Example:_

```bash
citeproc-cli -r '[[{"id": "citekey1", "prefix": "ct."}, {"id": "citekey2", "suffix": "p. 16"}], [{"id": "citekey1"}]]'
```

#### `--reffile`/`-p`

_Default:_ `null`

Path to a file containing the same JSON serialized data you would pass with `--references`/`-r`. While probably a little slower than the former, it's useful for bigger data sets or if you don't want to mess with escaping. This flag overrides `--keys`/`-k` and `--references`/`-r`.

#### `--uncited`/`-u`

_Default:_ `[]`

JSON-serialized list containing uncited keys to add to bibliography (without a preceding `@`). Either wrap in single quotes (which should always be possible since allowed characters in CSL keys are limited) or avoid white space. One list entry can contain multiple keys separated by `;`

_Example:_

```bash
citeproc-cli -u '["citekey1", "citekey2;citekey3", "citekey4"]'
```

#### `--nojson`/`-n`

_Default:_ `false`

Debugging option: Don't JSON-stringify output to make STDOUT a bit easier to read.

## Error codes

<dl>
    <dt><code>129</code></dt>
    <dd>Error reading a file from disk. Path of file is written to STDERR.</dd>
    <dt><code>130</code></dt>
    <dd>Citekey missing in data file. Key is written to STDERR.</dd>
</dl>

## Packaging

Run [pkg](https://www.npmjs.com/package/pkg) on the package:


```bash
npm install .
npm install --only=dev .
./node_modules/.bin/pkg .
```

Optionally, use the `--target` attribute to build for a system/architecture that is not mentioned in [package.json](./package.json).
