const fs = require("fs")
var os = require('os')


// Separator for multiple keys in one citation according to CSL standard
var CITEKEY_SEPARATOR = ";"


/**
 * Read text file from disk
 * Exit with code 129 on any error
 * @param  {String} path Path of the text file to read
 * @return {String} Read content of the file
 */
function fileReader(path) {
    try {
        filedata = fs.readFileSync(path, 'utf8')
        return filedata
    } catch (err) {
        console.error("Error reading " + path + os.EOL + err.toString())
        process.exit(129)
    }
}


/**
 * Get a complete CSLJSON object from citekey out of CSLJSON data set
 * @param {String} key citekey/id of the CSLJSON entry
 * @param {Array} csldata Array of all loaded CSLJSON objects
 * @return {Object, null} CSLJSON object of key or null if entry not found
 */
function getByKey(key, csldata) {
    for (const elem of csldata) {
        if (elem["id"] == key) return elem
    }
    return null
}


/**
 * Verify if all citekeys present in keys exist in CSLJSON data set
 * Exit with code 130 on missing key
 * @param {Array} keys List of citekeys to check
 * @param {Array} csldata Array of all loaded CSLJSON objects
 */
function verifyKeys(keys, csldata) {
    keyset = new Set(keys)
    for (const k of keyset) {
        let d = getByKey(k, csldata)
        if (d == null) {
            console.error("Citekey " + k + " missing in CSL data")
            process.exit(130)
        }
    }
}


/**
 * If a passed key contains a key separator, split into multiple citekeys
 * @param {String} multikey Citekey to split
 * @returns {Array} List of split keys (1-length array if separator not found)
 */
function separateKeys(multikey) {
    return multikey.split(CITEKEY_SEPARATOR)
}


/**
 * From citekey list, flags -k or -u: Build a list of citekeys where entries with multiple keys are flattend out
 * @param {Array} keys  List of citekeys, potentially containing multiple citekeys in one entry
 * @returns {Array} List of citekeys, flattend out to only contain one key per entry
 */
function sequenceKeys(keys) {
    let sequenced = []
    for(i of keys) {
        for(j of separateKeys(i)) {
            sequenced.push(j)
        }
    }
    return sequenced
}


/**
 * From reference list, flag -r: Build a list of citekeys where entries with multiple keys are flattend out
 * @param {Array} refs  Array of arrays of dictionaries (citationItems, ct. https://github.com/Juris-M/citeproc-js/blob/master/attic/citeproc-doc.rst#citation-data-object)
 * @returns {Array} List of citekeys, flattend out to only contain one key per entry
 */
function sequenceReferences(refs) {
    let sequenced = []
    for(i of refs) {
        for(j of i) {
            sequenced.push(j.id)
        }
    }
    return sequenced
}


module.exports = {
    fileReader: fileReader,
    getByKey: getByKey,
    verifyKeys: verifyKeys,
    separateKeys: separateKeys,
    sequenceKeys: sequenceKeys,
    sequenceReferences: sequenceReferences
}
