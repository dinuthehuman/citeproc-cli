const fs = require("fs")
const path = require('path');
const { fileReader, getByKey } = require("./utils.js")


module.exports = class SysObj {

    /**
     * Init sys object
     * @param {Array} csldata Array of all loaded CSLJSON objects
     * @param {String} localedir Path to folder containing locale xml files
     */
    constructor(csldata, localedir) {
        this.csldata = csldata;
        this.localedir = localedir;
    }

    /**
     * Get path of XML file containing locale data
     * @param {String} loc Locale string compliant with RFC 5646
     * @returns {String, null} Path of the XML file containig the locale data, null if no file matching the locale
     */
    getLocalePath(loc) {
        if (loc == "us") loc = "en-US"
        const files = fs.readdirSync(this.localedir)
        for (const locfile of files) {
            if (locfile.includes(loc)) {
                return path.join(this.localedir, locfile);
            }
        }
        return null;
    }

    /**
     * Get path of XML file containing locale data - required for citeproc-js sys object
     * @param {String} loc Locale string compliant with RFC 5646
     * @returns {String, boolean} XML string containing the locale data, false if no file matching the locale
     */
    retrieveLocale(loc) {
        let locfile = this.getLocalePath(loc)
        if (locfile == null) {
            return false
        }
        return fileReader(locfile)
    }

    /**
     * Get a complete CSLJSON object from citekey - required for citeproc-js sys object
     * @param {String} key citekey/id of the CSLJSON entry
     * @returns {Object} CSLJSON object of key
     */
    retrieveItem(key) {
        return getByKey(key, this.csldata)
    }
}
