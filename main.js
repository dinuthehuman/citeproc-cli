#!/usr/bin/env node
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers');
const SysObj = require("./sysobj.js")
const { fileReader, verifyKeys, separateKeys, sequenceKeys, sequenceReferences } = require("./utils.js");
var CSL = require("citeproc/citeproc_commonjs");

// Get & parse cli arguments
parsed_args = yargs(hideBin(process.argv))
    .option('data', {
        alias: 'd',
        description: 'Path to CLSJSON file containig bibliographic data'
    })
    .option('style', {
        alias: 's',
        description: "Path to CSL file"
    })
    .option('bibliography', {
        alias: 'b',
        description: "Render bibliography (omit to render footnotes)",
        type: 'boolean',
        default: false
    })
    .option('localedir', {
        alias: 'j',
        description: "Path to directory containing CSL locales",
        default: "./locales"
    })
    .option('locale', {
        alias: 'l',
        description: "Locale string compliant with RFC 5646",
        default: "en-US"
    })
    .option('keys', {
        alias: 'k',
        description: "JSON string containing all used citekeys in order (wrap in single quotes or avoid white space, overridden by `-r` and `-p`)",
        default: "[]"
    })
    .option('references', {
        alias: 'r',
        description: "JSON serialized array of dictionaries containing references (entries must contain key `id`, overrides `-k`, overridden by `-p`)",
        default: "[]"
    })
    .option('reffile', {
        alias: 'p',
        description: "Path to file containing JSON serialized references (overrides `-k` and `-r`)",
        default: null
    })
    .option('uncited', {
        alias: 'u',
        description: "JSON string containing all uncited citekeys (wrap in single quotes or avoid white space)",
        default: "[]"
    })
    .option('nojson', {
        alias: 'n',
        description: "Don't json-stringify output (for debugging)",
        type: "boolean",
        default: false
    })
    /* Option unnecessary: Currently, only html is supported
    .option('format', {
        alias: 'f',
        description: "Export format: html (defaults to html)",
        default: "html"
    })
    */
    .demandOption("data", "Input data file must be specified")
    .demandOption("style", "CSL file must be specified")
    .parse();

// Read data files
let csljson = JSON.parse(fileReader(parsed_args["data"]))
let cslfile = fileReader(parsed_args["style"])

// Load citekeys
let citekeys = JSON.parse(parsed_args["keys"])
let uncited = JSON.parse(parsed_args["uncited"])
let references = JSON.parse(parsed_args["references"])
if(parsed_args["reffile"] !== null) {
    references = JSON.parse(fileReader(parsed_args["reffile"]))
}

// Sequence keys for bibliography & verification
let seq_keys = []
if (references.length == 0) {
    seq_keys = sequenceKeys(citekeys);
}
else {
    seq_keys = sequenceReferences(references)
}
let seq_uncited = sequenceKeys(uncited)

// Verify if cited and uncited keys are present in the CSLJSON
verifyKeys(seq_keys, csljson)
verifyKeys(seq_uncited, csljson)

// Create sys object for CSL engine
sys = new SysObj(csljson, parsed_args["localedir"])

// Create engine
let citeproc = new CSL.Engine(sys, cslfile, parsed_args['locale'], true);

// Pass keys to registry for bibliography
citeproc.updateItems(seq_keys)
citeproc.updateUncitedItems(seq_uncited)

// Write bibliography & exit if necessary
if(parsed_args["bibliography"]) {
    if (parsed_args["nojson"]) console.log(citeproc.makeBibliography())
    else console.log(JSON.stringify(citeproc.makeBibliography()))
    process.exit(0);
}

// Loop through all citations and render them in order
// Build a stack of already processed citations and pass it to each subsequent processing run
let rendered_citations = []
let processed_tuples = []

// Flag -k is used as source of citations
if (references.length == 0) {
    for(i=0; i < citekeys.length; i++) {
        let citation = {
            properties: { noteIndex: i },
            citationItems: []
        }
        for (j of separateKeys(citekeys[i])) {
            citation.citationItems.push({id: j})
        }
        let result = citeproc.processCitationCluster(citation, processed_tuples, [], parsed_args["format"]);
        rendered_citations.push(result[1][0][1])
        processed_tuples.push([citation.citationID, i])
    }
}

// Flag -r is used as source of citations
else {
    for (i = 0; i < references.length; i++) {
        let citation = {
            properties: { noteIndex: i },
            citationItems: []
        }
        for (j of references[i]) {
            citation.citationItems.push(j)
        }
        let result = citeproc.processCitationCluster(citation, processed_tuples, [], parsed_args["format"]);
        rendered_citations.push(result[1][0][1])
        processed_tuples.push([citation.citationID, i])
    }
}

// Output to stdout and exit
if (parsed_args["nojson"]) console.log(rendered_citations)
else console.log(JSON.stringify(rendered_citations))
process.exit(0)
